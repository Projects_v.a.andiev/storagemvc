﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storage.Models
{
    public class IndexModel
    {
        public List<RecordModel> RecordList { get; set; }
    }
}
