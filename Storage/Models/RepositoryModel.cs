﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;

namespace Storage.Models
{
    public class RepositoryModel
    {
        public ConcurrentDictionary<string, RecordModel> Storage { get; } = new ConcurrentDictionary<string, RecordModel>();
        public RepositoryModel()
        {
            Storage["1"] = new RecordModel { Key = "1", Value = "2", ChangeDate = DateTime.Now.AddSeconds(20), CreateDate = DateTime.Now };
            Storage["sadsa"] = new RecordModel { Key = "sadsa", ChangeDate = DateTime.Now.AddSeconds(50), CreateDate = DateTime.Now };
            
        }
    }

    public class RecordModel
    {
        [Required(ErrorMessage = "Введите значение ключа")]
        [Display(Name = "Ключ")]
        public string Key { get; set; }

        [Display(Name = "Значение")]
        public string Value { get; set; }

        
        [Display(Name = "Количество обращений")]
        public long CountRequest { get; set; }

        [Display(Name = "Дата создания записи")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Дата редактирования записи")]
        public DateTime ChangeDate { get; set; }
    }
}
