﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Storage.Models;

namespace Storage.Controllers
{
    public class StorageInfoController : Controller
    {
        private readonly RepositoryModel _info;

        public StorageInfoController(RepositoryModel info)
        {
            _info = info;

        }
        public IActionResult Index()
        {
            return View(new IndexModel { RecordList = _info.Storage.Values.ToList() });
        }

        [HttpPost]
        public IActionResult Delete(string key, RecordModel value)
        {
            _info.Storage.TryRemove(key, out value);
            if (_info.Storage != null)
            {
                TempData["message"] = $"удалена запись c ключем {key} и значением {value}";
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ViewResult Edit(string key, DateTime createDate)
        {
            if (string.IsNullOrWhiteSpace( key))
            {
                return View(new RecordModel { CreateDate = DateTime.Now });
            }

            RecordModel record = _info.Storage.AddOrUpdate(
                key,
                (RecordModel)null,
                (k, v) => { ++v.CountRequest;  v.CreateDate = createDate; return v; });

            return View("Edit",record);
        }


        [HttpPost]
        public IActionResult Edit(RecordModel model)
        {
            if (ModelState.IsValid)
            {
                model.ChangeDate = DateTime.Now;

                

                _info.Storage.AddOrUpdate(model.Key, model, (k, v) => { model.CountRequest = v.CountRequest; model.CreateDate = v.CreateDate; return model; });

                if (_info.Storage != null)
                {
                    if (model.CreateDate == DateTime.MinValue)
                    {
                        model.CreateDate = DateTime.Now;
                    }
                    TempData["message"] = $"отредактирована запись c ключем {model.Key} и значением {model.Value} ";
                }

           

                return RedirectToAction("Index");
            }
            else
            {
                //Что-то не так со значениями данных
                return View();
            }
        }

        [HttpGet]
        public IActionResult ShowNotEmptyValue()
        {
            return View("Index", new IndexModel
            {
                RecordList = _info.Storage.Values.Where(p => !string.IsNullOrWhiteSpace(p.Value)).ToList()
            });
        }

        [HttpGet]
        public IActionResult ClearRecords()
        {
            _info.Storage.Clear();
            return View("Index", new IndexModel
            {
                RecordList = _info.Storage.Values.ToList()
            });
          
        }
    }



    //static class ExtensionMethods
    //{
    //    // Добавление или Обновление записи
    //    public static void AddOrUpdate<K, V>(this ConcurrentDictionary<K, V> dictionary, K key, V value)
    //    {
    //        dictionary.AddOrUpdate(key, value, (oldkey, oldvalue) => value);
    //    }
    //}
}