using Storage.Models;
using System;
using Xunit;
using System.Collections.Concurrent;
using System.Linq;

namespace StorageTest
{
    public class StorageTest
    {
        [Fact]
        public void Can_Add_New_Record()
        {
            //����������� ��������� ���� ������� � dictionary
            var allRecords = new RepositoryModel();

            //�������� - ���������� ����� ������.
            var newKey = "TestKey";
            allRecords.Storage.AddOrUpdate(newKey,
                 new RecordModel { Key = newKey, Value = "TestValue" },
                 (k, v) => { ++v.CountRequest; return v; });

            //����������� 
            Assert.Equal("TestKey", allRecords.Storage["TestKey"].Key);
            Assert.Equal("TestValue", allRecords.Storage["TestKey"].Value);
        }

        [Fact]
        public void Can_Clear_Records()
        {
            //����������� - ��������� ���� ������� �� dictionary
            var allRecords = new RepositoryModel();

            //�������� - ������� ���� �������� �� �������
            allRecords.Storage.Clear();

            //�����������
            Assert.Empty(allRecords.Storage);
        }

        [Fact]
        public void Can_Delete_Record()
        {
            //����������� - ��������� ���� ������� �� dictionary
            var allRecords = new RepositoryModel();

            //�������� - ���������� ����� ������.
            var newKey = "RemoveKey";
            allRecords.Storage.AddOrUpdate(newKey,
                     new RecordModel { Key = newKey, Value = "RemoveValue" },
                     (k, v) => { ++v.CountRequest; return v; });
            //�������� ���������� ������
            var deleteItem = allRecords.Storage["RemoveKey"];

            //�������� ������
            Assert.True(allRecords.Storage.TryRemove("RemoveKey", out deleteItem));

            //�������� �������� ������ ������� ��� ���
            Assert.False(allRecords.Storage.TryRemove("RemoveKey", out deleteItem));

        }


        [Fact]
        public void Show_Entries_That_Matter()
        {
            //����������� - ��������� ���� ������� �� dictionary
            var allRecords = new RepositoryModel();
            allRecords.Storage.Clear();

            //�������� - ���������� ������� ������� �������� � �� ������� ��������

            for (int i = 0; i < 5; i++)
            {
                allRecords.Storage.AddOrUpdate(i.ToString(),
                     new RecordModel
                     {
                         Key = i.ToString(),
                         Value = (i >=3) ? i + "Value": ""
                     },
                     (k, v) => { ++v.CountRequest; return v; });
            }
      
            //��� ������ � ������� ����������
            Assert.True(allRecords.Storage.Values.Where(p => !string.IsNullOrWhiteSpace(p.Value)).Count() == 2 );

            //��� � ������������ ����������
            Assert.True(allRecords.Storage.Values.Where(p => string.IsNullOrWhiteSpace(p.Value)).Count() == 3);
        }

    }
}


